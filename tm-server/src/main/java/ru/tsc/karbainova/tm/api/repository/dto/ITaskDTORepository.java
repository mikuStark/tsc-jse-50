package ru.tsc.karbainova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskDTORepository extends IDTORepository<TaskDTO> {
    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull TaskDTO findById(@Nullable String id);

    TaskDTO findByName(@Nullable String userId, @Nullable String name);

    void removeById(@Nullable String id);

    void remove(@NotNull TaskDTO entity);

    @NotNull List<TaskDTO> findAll();

    @NotNull List<TaskDTO> findAllByUserId(@Nullable String userId);

    @Nullable TaskDTO findByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable TaskDTO findByIndex(@Nullable String userId, @NotNull Integer index);

    void removeByName(@Nullable String userId, @Nullable String name);

    void removeByIdUserId(@Nullable String userId, @NotNull String id);

    void removeByIndex(@NotNull String userId, int index);

    int getCount();

    int getCountByUser(@NotNull String userId);

    void bindTaskById(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    void unbindTaskById(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    @NotNull List<TaskDTO> findTasksByUserIdProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    void removeTasksByProjectId(@Nullable String projectId);
}
