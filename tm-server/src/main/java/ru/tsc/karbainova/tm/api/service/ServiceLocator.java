package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.api.service.dto.*;
import ru.tsc.karbainova.tm.api.service.model.IUserServiceModel;

public interface ServiceLocator {
    ITaskService getTaskService();

    IProjectService getProjectService();

    ICommandService getCommandService();

    IUserService getUserService();

    IPropertyService getPropertyService();

    ISessionService getSessionService();

    IAdminUserService getAdminUserService();

    IProjectToTaskService getProjectToTaskService();

    IUserServiceModel getUserServiceModel();
}
