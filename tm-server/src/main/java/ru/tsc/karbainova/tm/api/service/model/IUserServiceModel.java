package ru.tsc.karbainova.tm.api.service.model;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserServiceModel {
    @SneakyThrows
    List<User> findAll();

    @SneakyThrows
    void addAll(Collection<User> collection);

    @SneakyThrows
    User findByLogin(@NonNull String login);

    @SneakyThrows
    User findById(@NonNull String id);

    @SneakyThrows
    User create(@NonNull String login, @NonNull String password);

    @SneakyThrows
    User create(@NonNull String login, @NonNull String password, @NonNull String email);

    @SneakyThrows
    User setPassword(@NonNull String userId, @NonNull String password);

    @SneakyThrows
    User updateUser(
            @NonNull String userId,
            @NonNull String firstName,
            @NonNull String lastName,
            @Nullable String middleName);

    @SneakyThrows
    User add(User user);

    @SneakyThrows
    void clear();
}
