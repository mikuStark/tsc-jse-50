package ru.tsc.karbainova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.AbstractDTOEntity;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractDTORepository<E extends AbstractDTOEntity> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void add(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.persist(entity);
    }

    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null) return;
        for (E item : collection) {
            add(item);
        }
    }

}


