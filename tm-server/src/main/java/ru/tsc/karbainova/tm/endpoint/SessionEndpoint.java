package ru.tsc.karbainova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.dto.ISessionService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class SessionEndpoint extends AbstractEndpoint {

    public SessionEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @WebMethod
    public void closeSession(@WebParam(name = "session") final SessionDTO session) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().close(session);
    }
}
