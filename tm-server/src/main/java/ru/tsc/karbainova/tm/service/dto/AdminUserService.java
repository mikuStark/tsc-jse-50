package ru.tsc.karbainova.tm.service.dto;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.karbainova.tm.api.service.dto.IAdminUserService;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.empty.*;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.dto.UserDTO;
import ru.tsc.karbainova.tm.repository.dto.UserDTORepository;
import ru.tsc.karbainova.tm.service.PropertyService;
import ru.tsc.karbainova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class AdminUserService extends AbstractService<UserDTO> implements IAdminUserService {

    public AdminUserService(IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public UserDTO removeUser(@Nullable final UserDTO user) {
        if (user == null) return null;
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.removeById(user.getId());
            entityManager.getTransaction().commit();
            return user;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO lockUserByLogin(@NonNull String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
            @Nullable final UserDTO user = userRepository.findByLogin(login);
            if (user == null) return null;
            user.setLocked(true);
            final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO unlockUserByLogin(@NonNull String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
            @Nullable final UserDTO user = userRepository.findByLogin(login);
            if (user == null) return null;
            user.setLocked(false);
            final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
            return userRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<UserDTO> collection) {
        if (collection == null) return;
        for (UserDTO i : collection) {
            add(i);
        }
    }

    public boolean isLoginExists(@NonNull final String login) {
        if (login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    @SneakyThrows
    public UserDTO findByLogin(@NonNull final String login) {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
            return userRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO create(@NonNull final String login, @NonNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO create(@NonNull final String login, @NonNull final String password, @NonNull final Role role) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new EmptyLoginException();
        final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IUserDTORepository userDTORepository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            userDTORepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO create(@NonNull final String login, @NonNull final String password, @NonNull final String email) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new EmptyLoginException();
        final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IUserDTORepository userDTORepository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            userDTORepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO setPassword(@NonNull final String userId, @NonNull final String password) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        IPropertyService propertyService = new PropertyService();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
            @Nullable final UserDTO user = userRepository.findById(userId);
            if (user == null) return null;
            final String hash = HashUtil.salt(propertyService, password);
            entityManager.getTransaction().begin();
            user.setPasswordHash(hash);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Override
    @SneakyThrows
    public UserDTO updateUser(
            @NonNull final String userId,
            @NonNull final String firstName,
            @NonNull final String lastName,
            @Nullable final String middleName) {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);

            UserDTO user = userRepository.findById(userId);
            if (user == null) throw new ProjectNotFoundException();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO add(UserDTO user) {
        if (user == null) throw new EmptyUserNotFoundException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NonNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
