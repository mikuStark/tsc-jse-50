package ru.tsc.karbainova.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.karbainova.tm.api.service.ILoggerService;
import ru.tsc.karbainova.tm.dto.LogDto;
import ru.tsc.karbainova.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;


public class JmsLogListener implements MessageListener {
    @NotNull
    final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof ObjectMessage)) return;
        @NotNull final Serializable entity = ((ObjectMessage) message).getObject();
        if (entity instanceof LogDto) loggerService.writeLog((LogDto) entity);
    }

}
