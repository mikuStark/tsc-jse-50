package ru.tsc.karbainova.tm.command.serv;

import com.jcabi.manifests.Manifests;
import ru.tsc.karbainova.tm.command.AbstractCommand;

public class AboutDisplayCommand extends AbstractCommand {
    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "About dev";
    }

    @Override
    public void execute() {
        System.out.println(serviceLocator.getPropertyService().getDeveloperName());
        System.out.println(Manifests.read("developer"));
        System.out.println(serviceLocator.getPropertyService().getDeveloperEmail());
    }
}
