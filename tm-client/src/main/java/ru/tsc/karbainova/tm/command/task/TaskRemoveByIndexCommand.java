package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

public class TaskRemoveByIndexCommand extends AbstractCommand {
    @Override
    public String name() {
        return "remove-by-index-task";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove by index";
    }

    @Override
    public void execute() {

        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        SessionDTO session = serviceLocator.getSession();
//        serviceLocator.getTaskEndpoint().removeByIndexTask(session, index);
    }
}
