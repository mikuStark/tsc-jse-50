package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.endpoint.ProjectDTO;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void clear();

    void addAll(List<ProjectDTO> projects);

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, ProjectDTO project);

    void remove(String userId, ProjectDTO project);

    List<ProjectDTO> findAll();

    List<ProjectDTO> findAll(String userId);

    List<ProjectDTO> findAll(String userId, Comparator<ProjectDTO> comparator);

    ProjectDTO updateById(String userId, String id, String name, String description);

    ProjectDTO updateByIndex(String userId, Integer index, String name, String description);

    ProjectDTO removeById(String userId, String id);

    ProjectDTO removeByIndex(String userId, Integer index);

    ProjectDTO removeByName(String userId, String name);

    ProjectDTO findById(String userId, String id);

    ProjectDTO findByIndex(String userId, Integer index);

    ProjectDTO findByName(String userId, String name);

    ProjectDTO startById(String userId, String id);

    ProjectDTO startByIndex(String userId, Integer index);

    ProjectDTO startByName(String userId, String name);

    ProjectDTO finishById(String userId, String id);

    ProjectDTO finishByIndex(String userId, Integer index);

    ProjectDTO finishByName(String userId, String name);
}
